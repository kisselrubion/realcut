﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealCutApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RealCutApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReportedCasesPage : ContentPage
	{
		public ReportedCasesPage (User user)
		{
			InitializeComponent ();
			BindingContext = App.Locator.ReportsViewModel;
		}
	}
}