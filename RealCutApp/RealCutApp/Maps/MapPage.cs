﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Plugin.Geolocator;
using RealCutApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace RealCutApp.Maps
{
	public class MapPage : ContentPage
	{
		public ReportsViewModel vm = App.Locator.ReportsViewModel;
		public Map Map;
		public MapPage()
		{
			Title = "iDenguefy";
			BindingContext = vm;
			Map = new Map
			{
				IsShowingUser = true,
				HeightRequest = 100,
				WidthRequest = 960,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			var position1 = new Position(7.0725355, 125.612297);
			var pin = new Pin
			{
				Type = PinType.Place,
				Position = position1,
				Label = "Dengue Outbreak",
			};
			Map.Pins.Add(pin);
			Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(7.0725355, 125.612297), Distance.FromMiles(0.3)));
			var stack = new StackLayout { Spacing = 0 };
			stack.Children.Add(Map);
			Content = stack;
		}

		protected override void OnAppearing()
		{
			//Map.Pins.Add(new Pin
			//{
			//	Type = PinType.Place,
			//	Position = new Position(vm.CurrentPosition.Latitude, vm.CurrentPosition.Longitude),
			//	Label = vm.Report.Title,
			//	Address = vm.Report.Address,
			//});
		}
	}
}