﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RealCutApp.Models;
using Xamarin.Essentials;

namespace RealCutApp.Services.ReportService
{
    public class ReportService : IReportService
    {
	    public Task<bool> PostReportAsync(Report report)
	    {
			throw new NotImplementedException();
		}

		public Task<Report> GetReportAsync(Report report)
	    {
		    throw new NotImplementedException();
	    }

	    public async Task<Location> GetCurrentLocationAsync()
	    {
		    var request = new GeolocationRequest(GeolocationAccuracy.Medium);

			return await Geolocation.GetLocationAsync(request);
	    }
    }
}
