﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RealCutApp.Models;
using Xamarin.Essentials;

namespace RealCutApp.Services.ReportService
{
	public interface IReportService
	{
		Task<bool> PostReportAsync(Report report);
		Task<Report> GetReportAsync(Report report);
		Task<Location> GetCurrentLocationAsync();
	}
}
