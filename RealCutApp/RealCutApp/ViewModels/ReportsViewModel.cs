﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using RealCutApp.Helpers;
using RealCutApp.Models;
using RealCutApp.Services.ReportService;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;

namespace RealCutApp.ViewModels
{
	public class ReportsViewModel : RealCutBaseViewModel
	{
		#region Fields and Properties
		private User _user;
		private Report _report;
		private Location _currentPosition;
		private readonly IReportService _reportService;
		private List<string> _selectedSymptoms;
		private string _selectedSeverity;
		public User User
		{
			get => _user;
			set => Set(ref _user, value);
		}
		public Report Report
		{
			get => _report;
			set
			{
				_report = value;
				Report.DateReported = DateTime.Now;
				RaisePropertyChanged(() => Report);
			}
		}
		public Location CurrentPosition
		{
			get => _currentPosition;
			set => Set(ref _currentPosition, value);
		}
		public string SelectedSeverity
		{
			get => _selectedSeverity;
			set => Set(ref _selectedSeverity, value);
		}
		public ObservableCollection<string> SeverityTypesList { get; set; }
		public ObservableCollection<Symptom> SymptomsList { get; set; }
		public ObservableCollection<Report> ReportedCasesList { get; set; }
		#endregion

		#region Ctor
		public ReportsViewModel(IReportService reportService)
		{
			_reportService = reportService;
			User = new User
			{
				FirstName = "Jack",
				LastName = "Smith"
			};
			Report = new Report();
			CurrentPosition = new Location();
			ReportedCasesList = new ObservableCollection<Report>();
			GenerateSeverityTypeList();
			GenerateSymptomsList();
			Task.Run(GenerateCurrentLocation);
		}
		#endregion

		#region Submitting Report
		public ICommand PostReportCommand => new RelayCommand(() => PostReport());
		private void PostReport()
		{
			Report.Severity = SelectedSeverity;
			Report.SymptomsList = SymptomsList.Where(s => s.IsSelected).ToList();
			Report.UserReported = User;
			//ReportedCasesList.Add(Report);
			//NavigationService.NavigateTo(Locator.ReportedCasesPage, User, true);
			//await _reportService.PostReportAsync(Report);
		}
		#endregion

		#region Generate Elements
		private void GenerateSeverityTypeList()
		{
			SeverityTypesList = new ObservableCollection<string> { "Stage 1", "Stage 2", "Stage 3", "Stage 4" };
		}

		private void GenerateReportedCases()
		{

		}
		private void GenerateSymptomsList()
		{
			SymptomsList = new ObservableCollection<Symptom>
			{
				new Symptom {SymptomName = "Headache"},
				new Symptom {SymptomName = "Myalgia"},
				new Symptom {SymptomName = "Vomiting"},
				new Symptom {SymptomName = "Cough"},
				new Symptom {SymptomName = "Abdominal Pain"},
				new Symptom {SymptomName = "Rash"},
				new Symptom {SymptomName = "Any bleeding episodes"}
			};
		}
		private async Task GenerateCurrentLocation()
		{
			CurrentPosition = await _reportService.GetCurrentLocationAsync();
			if (CurrentPosition != null)
			{
				User.CurrentLatitude = CurrentPosition.Latitude;
				User.CurrentLongitude = CurrentPosition.Longitude;
			}
		}
		#endregion


	}
}
