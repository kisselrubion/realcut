﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using RealCutApp.Helpers;
using RealCutApp.Models;
using RealCutApp.Services;
using Xamarin.Forms;

namespace RealCutApp.ViewModels
{
	public class MasterDetailItem 
	{
		public string Title { get; set; }
		public string IconClass { get; set; }
	}
	public class MenuViewModel : RealCutBaseViewModel
	{
		private bool _setRoot;
		private User User { get; }
		public List<MasterDetailItem> MenuItems { get; set; }

		public MenuViewModel()
		{
			User = new User();
		}

		public ICommand GotoBookingPageCommand => new RelayCommand(GotoBookingPageProc);

		private void GotoBookingPageProc()
		{
			_setRoot = NavigationService.CurrentPageKey == Locator.BookingPage;
			NavigationService.NavigateTo(Locator.BookingPage, User, _setRoot);
			ToggleMasterPageIsPresented();
		}

		public ICommand GotoReportedCasesCommand => new RelayCommand(GotoReportedCasesProc);

		private void GotoReportedCasesProc()
		{
			_setRoot = NavigationService.CurrentPageKey == Locator.ReportedCasesPage;
			NavigationService.NavigateTo(Locator.ReportedCasesPage, User, _setRoot);
			ToggleMasterPageIsPresented();
		}

		public ICommand GotoMapCommand => new RelayCommand(GotoMapProc);

		private void GotoMapProc()
		{
			_setRoot = NavigationService.CurrentPageKey == Locator.MapPage;
			NavigationService.NavigateTo(Locator.MapPage, null, true);
			ToggleMasterPageIsPresented();
		}

		private void ToggleMasterPageIsPresented()
		{
			((MasterDetailPage)(Application.Current.MainPage)).IsPresented = !((MasterDetailPage)(Application.Current.MainPage)).IsPresented;
		}
	}
}
