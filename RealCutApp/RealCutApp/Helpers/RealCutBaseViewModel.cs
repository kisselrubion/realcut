﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using RealCutApp.Services;
using RealCutApp.Services.Navigation;

namespace RealCutApp.Helpers
{
    public class RealCutBaseViewModel : ViewModelBase
    {
	    bool isBusy;
	    public NavigationService navigationService;
	    public string Name { get; set; }
	    public RealCutBaseViewModel()
	    {
		    navigationService = App.NavigationService;
	    }
	    public NavigationService NavigationService
	    {
		    get { return navigationService; }
	    }
	    public bool IsBusy
	    {
		    get { return isBusy; }
		    set { Set(ref isBusy, value); }
	    }
	    /// <summary>
	    /// Function which initializes data needed to be displayed in the view. To be called on the OnAppearing method of the view.
	    /// </summary>
	    /// <returns>The async.</returns>
	    /// <param name="navigationData">Navigation data.</param>
	    public virtual Task InitializeAsync(object navigationData)
	    {
		    return Task.FromResult(false);
	    }

	    /// <summary>
	    /// Function which resets data in the view. Use this for logouts instances
	    /// </summary>
	    /// <returns>The async.</returns>
	    /// <param name="navigationData">Navigation data.</param>
	    /// 
	    public virtual Task ResetAsync()
	    {
		    return Task.FromResult(false);
	    }
	}
}
