﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using RealCutApp.Maps;
using RealCutApp.Models;
using RealCutApp.Services;
using RealCutApp.Services.Navigation;
using RealCutApp.Views;
using Xamarin.Forms;

namespace RealCutApp.Helpers
{
	public class InitializeNavigation
	{
		public NavigationService NavigationService { get; }

		public InitializeNavigation()
		{
			NavigationService = new NavigationService();
			SimpleIoc.Default.Reset();
			SimpleIoc.Default.Register<INavigationService>(() => NavigationService);
		}

		public void InitializePages()
		{
			NavigationService.Configure(Locator.DashboardPage, typeof(DashboardPage));
			NavigationService.Configure(Locator.BookingPage, typeof(BookingPage));
			NavigationService.Configure(Locator.ReportedCasesPage, typeof(ReportedCasesPage));
			NavigationService.Configure(Locator.MapPage, typeof(MapPage));
		}

		public MasterDetailPage SetDetailPage()
		{
			var masterPage = new NavigationPage(new BookingPage(new User()));
			var masterclasses = new MasterDetailPage()
			{
				Detail = masterPage,
				Master = new MenuPage()
				{
					Title = "Menu"
				},
			};
			NavigationService.Initialize(masterPage);
			return masterclasses;
		}
	}
}
