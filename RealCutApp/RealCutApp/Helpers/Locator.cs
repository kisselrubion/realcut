﻿using System;
using System.Collections.Generic;
using System.Text;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using RealCutApp.Services.ReportService;
using RealCutApp.ViewModels;
using Xamarin.Forms;

namespace RealCutApp.Helpers
{
	public class Locator
	{
		public const string DashboardPage = "DashboardPage";
		public const string BookingPage = "BookingPage";
		public const string ReportedCasesPage = "ReportedCasesPage";
		public const string MapPage = "MapPage";

		public Locator()
		{
			ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
			SimpleIoc.Default.Register<MenuViewModel>();
			SimpleIoc.Default.Register<ReportsViewModel>();

			//Dependency Injection
			SimpleIoc.Default.Register<IReportService,ReportService>();
		}
		public MenuViewModel MenuViewModel => ServiceLocator.Current.GetInstance<MenuViewModel>();
		public ReportsViewModel ReportsViewModel => ServiceLocator.Current.GetInstance<ReportsViewModel>();

	}
	public static class ViewModelLocator
	{
		public static Locator Locator = (Locator)Application.Current.Resources["Locator"];
	}
}
