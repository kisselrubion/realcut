﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealCutApp.Models
{
    public class Report
    {
		public int ReportId { get; set; }
		public string Title { get; set; }
		public string Message { get; set; }
		public string Address { get; set; }
	    public DateTime DateReported { get; set; }
	    public User UserReported { get; set; }
		public string Severity { get; set; }
		public List<Symptom> SymptomsList { get; set; }
		public bool IsBeingDealtWith { get; set; }
    }
}
