﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealCutApp.Models
{
	public class User
	{
		public int UserId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string CurrentAddress { get; set; }
		public string ContactNumber { get; set; }
		public string Age { get; set; }
		public double CurrentLongitude { get; set; }
		public double CurrentLatitude { get; set; }
	}
}
