﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealCutApp.Models
{
	public class Symptom
	{
		public string SymptomName { get; set; }
		public bool IsSelected { get; set; }
	}
}
