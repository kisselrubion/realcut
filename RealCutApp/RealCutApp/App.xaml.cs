using System;
using RealCutApp.Helpers;
using RealCutApp.Services;
using RealCutApp.Services.Navigation;
using RealCutApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace RealCutApp
{
	public partial class App : Application
	{
		public static Locator Locator;
		public static NavigationService NavigationService { get; set; }
		private readonly InitializeNavigation _initializeNavigation;
		public App ()
		{
			InitializeComponent();
			if (_initializeNavigation == null)
			{
				_initializeNavigation = new InitializeNavigation();
			}
			Locator = new Locator();
			NavigationService = _initializeNavigation.NavigationService;
			_initializeNavigation.InitializePages();
			MainPage = _initializeNavigation.SetDetailPage();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
